package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.*;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractServiceEndpoint<Task> implements ITaskEndpoint {
    public TaskEndpoint(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Task> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public int checkTaskSize(@NotNull Session session) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return 0;

        return getRepository().getCollection().size();
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull Session session, @NotNull final String name) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        Task result = null;
        if (Objects.isNull(getRepository()))
            return result;


        for (@NotNull final Task task : getRepository().getCollection()) {
            if (task.getName().equals(name)) {
                result = task;
                break;
            }
        }
        return result;
    }

    @Override
    public void removeTask(@NotNull Session session, @NotNull final Task task) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;

        getRepository().remove(task);
    }

    @Override
    public void removeAllTasksByUserId(@NotNull Session session, @NotNull final String userId) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;

        getRepository().removeAll(userId);
    }

    @Override
    public void removeAllTasks(@NotNull Session session, @NotNull final Collection<Task> listTasks) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;
        getRepository().removeAll(listTasks);
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull Session session, @NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.nonNull(name) && Objects.nonNull(getRepository())){
            Task newTask = (Task) task.clone();
            newTask.setName(name);
            return getRepository().merge(newTask);
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull Session session, @NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException, ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.nonNull(project) && Objects.nonNull(getRepository())){
            Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            getRepository().merge(newTask);
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasksByUserId(@NotNull Session session, @NotNull final String userId, @NotNull final String projectId) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();
        return ((ITaskRepository)getRepository()).findAll(userId, projectId);
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(@NotNull Session session, String userId) throws ValidateExeption {
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();
        return ((ITaskRepository) getRepository()).findAllByUser(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasks(@NotNull Session session, @NotNull final String projectId) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();
        return getRepository().findAll();
    }

    @Override
    public void putTask(@NotNull Session session, @NotNull final Task task) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;
        getRepository().merge(task);
    }

    @Override
    public @NotNull MyMap getTaskMapRepository() {
        return new MyMap(getRepository().getMapRepository());
    }

    @Override
    public void loadBinar(@NotNull Session session) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
    }

    @Override
    public void saveBinar(@NotNull Session session) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
    }

    @Override
    public void setTaskMapRepository(@NotNull final MyMap mapRepository){
        getRepository().setMapRepository((Map<String, Task>) mapRepository.getMap());
    };

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull Session session, @NotNull final Task task) throws ValidateExeption {
        getServiceLocator().getSessionEndpoint().validSession(session);
        String res = "";
        if (Objects.isNull(task.getDateStart()) || Objects.isNull(task.getDateFinish()) || Objects.isNull(getServiceLocator())
       )
            return res;

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Project " + getServiceLocator().getProjectService().findOneProjectById(session, task.getProjectId())  + "]" +  "\n";
        res += "    Status: " + task.getStatus() + "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " +  getServiceLocator().getFt().format(task.getDateStart() ) + "\n";
        res += "    Date finish: " + getServiceLocator().getFt().format(task.getDateFinish() ) + "\n";

        return res;
    }
    //Save-Load
    @Override public void saveJaxb(@NotNull Session session, boolean formatXml) throws Exception { }
    @Override public void loadJaxb(@NotNull Session session, boolean formatXml) throws Exception {}

    @Override public void saveFasterXml(@NotNull Session session) throws Exception { }
    @Override public void loadFasterXml(@NotNull Session session) throws Exception {}

    @Override public void saveFasterJson(@NotNull Session session) throws Exception { }
    @Override public void loadFasterJson(@NotNull Session session) throws Exception {}
}
