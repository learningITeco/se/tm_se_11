package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.Session;
import ru.potapov.tm.endpoint.User;

import javax.xml.bind.DatatypeConverter;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class UserAuthorizeCommand extends AbstractCommand {
    private IUserService userService = null;

    public UserAuthorizeCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        setNeedAuthorize(false);
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Logins user in Task-manager";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @NotNull String userInput = "";
        while ( true ){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Type your login name or the command <exit>");
            if ("exit".equals(userInput)){
                return;
            }

            userInput = getServiceLocator().getTerminalService().readLine("login: ");
            @NotNull String userName = userInput;
            userInput = getServiceLocator().getTerminalService().readLine("pass: ");
            @NotNull String hashPass = userInput;


//            @NotNull Console console = System.console();
//            char[] chUserInput = console.readPassword("pass:");//getBootstrap().getIn().;
//            System.out.print("pass: ");
//            System.out.println(chUserInput.toString());
//            userInput = chUserInput.toString();
            User user = getServiceLocator().getUserService().getUserByNamePass(userName, hashPass);
            if ( Objects.isNull(user) ){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed the user name or password!");
                continue;
            }

            getServiceLocator().getSessionService().setSession(user.getSession());
            getServiceLocator().getUserService().setAuthorized(true);
            getServiceLocator().getUserService().setAuthorizedUser(user);
            getServiceLocator().getTerminalService().printMassageCompleted();
            return;
        }
    }
}
