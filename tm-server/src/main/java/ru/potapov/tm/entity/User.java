package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class User extends Entity{
    private final static long     serialVersionUID = 1l;

    @Nullable private String      login;
    @Nullable private String      hashPass;
    @Nullable private Session     session;
    @NotNull  private RoleType    roleType = RoleType.User;

    public User(@NotNull String login) {
        this();
        this.login = login;
    }

    @Nullable
    @Override
    public String getName() {
        return login;
    }

    @Nullable
    @Override
    public String getUserId() {
        return getId();
    }

    @NotNull
    public String displayName(){
        return "" + roleType + ": " + login;
    }

    @NotNull
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
