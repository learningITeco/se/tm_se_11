package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

@Getter
@Setter
@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {
    public ExitCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(false);
    }

    @NotNull
    @Override
    public String getName() {
        setNeedAuthorize(false);
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Halts the app";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        System.out.println("Buy-buy....");
        //Save our users
        getServiceLocator().getUserService().saveAllUsers();
    }
}
