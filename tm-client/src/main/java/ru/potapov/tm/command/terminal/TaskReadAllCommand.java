package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.endpoint.Task;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class TaskReadAllCommand extends TaskReadCommandAbstract {
    public TaskReadAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        setNeedAuthorize(false);
        return "task-read-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads all tasks. Sorts by create[-c], by date start[-ds], by date finish[-df], by date status[-s]. Find by part of name/descript [-f [part of seeking] ]";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        super.execute(additionalCommands);
        if (Objects.isNull(getServiceLocator()) || Objects.isNull(getServiceLocator().getUserService().getAuthorizedUser()))
            return;

        if (getServiceLocator().getTaskService().checkSize() == 0) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any tasks");
            return;
        }

        @NotNull final List<Task> list = (List<Task>) getServiceLocator().getTaskService().findAllByUser( getServiceLocator().getUserService().getAuthorizedUser().getId());
        Collections.sort(list, getComparator(sortBy));
        printTasks( list );

        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
