package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.User;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class UserReadCommand extends AbstractCommand {
    public UserReadCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-read";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads a user in detail";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @Nullable User findUser = null;
        boolean circleForProject = true;
        while (circleForProject) {
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a user name:");

            if ("exit".equals(name)){
                return;
            }

            findUser = getServiceLocator().getUserService().getUserByName(name);

            if (Objects.isNull(findUser)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("User with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }
        String userInfo = getServiceLocator().getUserService().collectUserInfo(findUser);
        getServiceLocator().getTerminalService().printlnArbitraryMassage(userInfo);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
