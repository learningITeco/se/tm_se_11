package ru.potapov.tm.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.Data;
import ru.potapov.tm.dto.DataXml;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

@WebService(endpointInterface = "ru.potapov.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractServiceEndpoint<Project> implements IProjectEndpoint {
    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Project> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public @NotNull MyMap getProjectMapRepository() {
        return new MyMap(getRepository().getMapRepository());
    }

    @Override
    public int checkProjectSize(@NotNull Session session) throws ValidateExeption {
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()) )
            return 0;
        return getRepository().getCollection().size();
    }

    @Override
    public @Nullable Project findOneProject(@NotNull Session session, @NotNull String name) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOne(name);
    }

    @Override
    public @Nullable Project findOneProjectById(@NotNull Session session, @NotNull String id) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOneById(id);
    }

    @Override
    public @Nullable Project findOneProjectByIdAndUserId(@NotNull Session session, @NotNull String userId, @NotNull String id) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOneById(userId, id);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull Session session, @NotNull final String name) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOne(name);
    }

    @Nullable
    @Override
    public Project findProjectByNameAndUserId(@NotNull Session session, @NotNull final String userId, @NotNull final String name) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOne(userId, name);
    }

    @NotNull
    @Override
    public Collection<Project> getProjectCollection(@NotNull Session session, @NotNull final String userId) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        User user = getServiceLocator().getUserService().getUserById(session, userId);
        if ( getServiceLocator().getUserService().isAdministrator(session, user) ){
            return getRepository().getCollection();
        }
        return getRepository().getCollection(userId);
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull Session session, @NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException, ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.nonNull(name) && Objects.nonNull(getRepository()) ){
            Project newProject = (Project) project.clone();
            newProject.setName(name);
            return getRepository().merge(newProject);
        }
        return project;
    }

    @Override
    public void removeAllProjectByUserId(@NotNull Session session, @NotNull final String userId) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;
        getRepository().removeAll(userId);
    }
    
    @Override
    public void removeAllProject(@NotNull Session session, @NotNull final Collection<Project> listProjects) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;
        getRepository().removeAll(listProjects);
    }
    
    @Override
    public void removeProject(@NotNull Session session, @NotNull final Project project) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;
        getRepository().remove(project);
    }
    
    @Override
    public void putProject(@NotNull Session session, @NotNull final Project project) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        if (Objects.isNull(getRepository()))
            return;
        getRepository().merge(project);
    }

    @Override
    public void setProjectMapRepository(@NotNull final MyMap mapRepository){
        getRepository().setMapRepository(( Map<String, Project> )mapRepository.getMap());
    };

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull Session session, @NotNull final Project project, @NotNull final String owener) throws ValidateExeption{
        getServiceLocator().getSessionEndpoint().validSession(session);
        String res = "";

        if (Objects.isNull(project.getDateStart()) || Objects.isNull(project.getDateFinish()))
            return res;

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Status: " + project.getStatus() + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + getServiceLocator().getFt().format(project.getDateStart() ) + "\n";
        res += "Date finish: " +  getServiceLocator().getFt().format(project.getDateFinish()) + "\n";

        return res;
    }

    //Save-Load
    @Override
    public void saveBinar(@Nullable Session session) throws Exception{
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));

        @NotNull final Data data = new Data();
        data.setProjectMap( (Map<String, Project>)getServiceLocator().getProjectService().getProjectMapRepository().getMap() );
        data.setTaskMap( (Map<String, Task>) getServiceLocator().getTaskService().getTaskMapRepository().getMap() );

        inputStream.writeObject( data );
        inputStream.close();
    }

    @Override
    public void loadBinar(@Nullable Session session) throws Exception{
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        if (!file.canRead()){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream( new FileInputStream(file) );
        @NotNull final Object dataObj = inputStream.readObject();
        if (dataObj instanceof Data) {
            @NotNull final Data data = (Data) dataObj;
            getServiceLocator().getProjectService().setProjectMapRepository( new MyMap(data.getProjectMap()) );
            getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(data.getTaskMap()) );
        }
    }
    @Override
    public void saveJaxb(@Nullable Session session, boolean formatXml) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final Data data = new Data() ;
        data.setProjectMap( (Map<String, Project>) getServiceLocator().getProjectService().getProjectMapRepository().getMap() );
        data.setTaskMap( (Map<String, Task>)getServiceLocator().getTaskService().getTaskMapRepository().getMap() );

        @NotNull final JAXBContext context      = JAXBContext.newInstance(Data.class) ;
        @NotNull final Marshaller marshaller    = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        String typeFormatName = "xml";
        if (!formatXml){
            typeFormatName = "json";
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        }
        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        marshaller.marshal(data, new FileWriter(file));
    }
    @Override
    public void loadJaxb(@Nullable Session session, boolean formatXml) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Data.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();

        String typeFormatName = "xml";
        if (!formatXml) {
            typeFormatName = "json";
            unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        }

        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        @NotNull final Data data =(Data) unmarshaller.unmarshal(file);

        if (data.getProjectMap().size() > 0)
            getServiceLocator().getProjectService().setProjectMapRepository( new MyMap(data.getProjectMap()) );
        if (data.getTaskMap().size() > 0)
            getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(data.getTaskMap()) );
    }

    @Override
    public void saveFasterXml(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
        DataXml dataXml = new DataXml();
        dataXml.getListKeyProject().addAll(getProjectMapRepository().getMap().keySet());
        dataXml.getListValueProject().addAll((Collection<? extends Project>) getProjectMapRepository().getMap().values());
        dataXml.getListKeyTask().addAll(getServiceLocator().getTaskService().getTaskMapRepository().getMap().keySet());
        dataXml.getListValueTask().addAll((Collection<? extends Task>) getServiceLocator().getTaskService().getTaskMapRepository().getMap().values());

        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper  mapper = new XmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dataXml);
    }
    @Override
    public void loadFasterXml(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);

        Map<String, Project> mapProject = new HashMap<>();
        Map<String, Task> mapTask       = new HashMap<>();
        if ( (data.getListKeyProject().size() > 0) && (data.getListValueProject().size() > 0) ){
            for (int i = 0; i < data.getListKeyProject().size(); i++) {
                mapProject.put(data.getListKeyProject().get(i), data.getListValueProject().get(i));
            }
            getServiceLocator().getProjectService().setProjectMapRepository( new MyMap(mapProject) );
        }

        if ( (data.getListKeyTask().size() > 0) && (data.getListValueTask().size() > 0) ){
            for (int i = 0; i < data.getListKeyTask().size(); i++) {
                mapTask.put(data.getListKeyTask().get(i), data.getListValueTask().get(i));
            }
            getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(mapTask) );
        }
    }

    @Override
    public void saveFasterJson(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final Data data = new Data() ;
        data.setProjectMap( (Map<String, Project>)getProjectMapRepository().getMap() );
        data.setTaskMap( (Map<String, Task>) getServiceLocator().getTaskService().getTaskMapRepository().getMap() );

        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(s.getBytes());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
    @Override
    public void loadFasterJson(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionEndpoint().validSession(session);
        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Data data = mapper.readValue(file, Data.class);

        if (data.getProjectMap().size() > 0) {
            getRepository().getMapRepository().clear();
            getRepository().getMapRepository().putAll( data.getProjectMap() );
        }
        if (data.getTaskMap().size() > 0){
            getServiceLocator().getTaskService().getTaskMapRepository().getMap().clear();
            getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(data.getTaskMap()) );
        }
    }
}
