package ru.potapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.MessageDigest;
import java.util.Collection;

@WebService
public interface IUserEndpoint {
    //@WebMethod
    void setUserMapRepository(@NotNull final MyMap mapRepository);
    //@WebMethod
    @NotNull MyMap getUserMapRepository();
    @NotNull User createUser(@NotNull final String name, @NotNull final String hashPass, @NotNull final RoleType role);
    @Nullable User getUserByName(@NotNull final String name);
    @WebMethod @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception;
    @WebMethod @Nullable User getUserById(@WebParam @NotNull Session session, @NotNull final String id) throws ValidateExeption;
    boolean isUserPassCorrect(@NotNull final User user, @NotNull final String hashPass);
    //@WebMethod @NotNull Collection<User> getUserCollection(@WebParam @NotNull Session session);
    @NotNull Collection<User> getUserCollection();
    @WebMethod @NotNull User changePass(@WebParam @NotNull Session session, @NotNull final User user, @NotNull final String newHashPass) throws CloneNotSupportedException, ValidateExeption;
    @WebMethod void putUser(@WebParam @NotNull Session session, @NotNull final User user) throws ValidateExeption;
    @WebMethod @NotNull String collectUserInfo(@WebParam @NotNull Session session, @NotNull final User user) throws ValidateExeption;
    void createPredefinedUsers();
    void setAuthorizedUser(@NotNull final User authorizedUser);
    void saveAllUsers() throws Exception;
    void loadAllUsers() throws Exception;
    int sizeUserMap();
    @WebMethod @Nullable RoleType getUserRole(@WebParam @NotNull Session session, @NotNull final User user) throws ValidateExeption;
    @WebMethod boolean isAdministrator(@WebParam @NotNull Session session, @WebParam @NotNull final User user) throws ValidateExeption;
}
