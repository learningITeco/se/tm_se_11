package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;

import java.security.MessageDigest;
import java.util.Collection;
import java.util.Map;

public interface IUserService {
    void setMapRepository(@NotNull final Map<String, User> mapRepository);
    @NotNull Map<String, User> getMapRepository();
    @NotNull User create(@NotNull final String name, @NotNull final String hashPass, @NotNull final RoleType role);
    @Nullable User getUserByName(@NotNull final String name);
    @Nullable User getUserById(@NotNull final String id);
    boolean isUserPassCorrect(@NotNull final User user, @NotNull final String hashPass);
    @NotNull Collection<User> getCollectionUser();
    @NotNull User changePass(@NotNull final User user, @NotNull final String newHashPass) throws CloneNotSupportedException;
    void put(@NotNull final User user);
    @NotNull String collectUserInfo(@NotNull final User user);
    void createPredefinedUsers();
    @Nullable MessageDigest getMd();
    boolean isAuthorized();
    void setAuthorized(boolean authorized);
    @Nullable User getAuthorizedUser();
    void setAuthorizedUser(@NotNull final User authorizedUser);
    void saveAllUsers() throws Exception;
    void loadAllUsers() throws Exception;
    int size();
}
