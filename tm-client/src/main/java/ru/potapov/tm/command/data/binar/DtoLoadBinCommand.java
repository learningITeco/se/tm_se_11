package ru.potapov.tm.command.data.binar;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.User;

import java.util.Objects;

@NoArgsConstructor
public class DtoLoadBinCommand extends AbstractCommand {
    public DtoLoadBinCommand(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        setNeedAuthorize(true);
        return "load-binar";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in binary file";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        @Nullable final User user = getServiceLocator().getUserService().getAuthorizedUser();
        if (Objects.isNull(user) || !getServiceLocator().getUserService().isAdministrator( user ))
            return;
        if (!super.allowedRun())
            return;

        getServiceLocator().getProjectService().loadBinar(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTaskService().loadBinar(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
