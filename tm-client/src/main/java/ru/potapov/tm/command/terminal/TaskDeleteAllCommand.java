package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDeleteAllCommand extends AbstractCommand {
    public TaskDeleteAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-delete-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes all tasks of all projects";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()) || Objects.isNull(getServiceLocator().getUserService().getAuthorizedUser()))
            return;

        getServiceLocator().getTaskService().removeAll(getServiceLocator().getUserService().getAuthorizedUser().getId());
        getServiceLocator().getTerminalService().printlnArbitraryMassage("All tasks of all projects have deleted");
    }
}
