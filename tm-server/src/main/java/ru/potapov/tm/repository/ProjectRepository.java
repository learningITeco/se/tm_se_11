package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IProjectRepository;

import java.util.*;

import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.entity.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository<Project> {
    @NotNull
    @Override
    public Collection<Project> findAll() {
        return super.findAll();
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        return super.findAll(userId);
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String name) {
        return super.findOne(name);
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String name) {
        return super.findOne(userId, name);
    }

    @Override
    public void persist(@NotNull final Project project) {
        super.persist(project);
    }

    @Override
    public void persist(@NotNull final String userId, @NotNull final Project project) {
        super.persist(userId, project);
    }

    @NotNull
    @Override
    public Project merge(@NotNull final Project project) {
        return super.merge(project);
    }

    @NotNull
    @Override
    public Project merge(@NotNull final String userId, @NotNull final Project project) {
        return super.merge(userId, project);
    }

    @Override
    public void remove(@NotNull final Project project) {
        super.remove(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        super.remove(userId, project);
    }

    @Override
    public void removeAll() {
        super.removeAll();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        super.removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Project> list) {
        super.removeAll(list);
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final Collection<Project> list) {
        super.removeAll(userId, list);
    }

    @NotNull
    @Override
    public Collection<Project> getCollection(){
        return super.getCollection();
    }

    @NotNull
    @Override
    public Collection<Project> getCollection(@NotNull final String userId){
        Collection<Project> collections = new ArrayList<>();
        for (@NotNull final Project t : getMapRepository().values()) {
            if (t.getUserId().equals(userId))
                collections.add(t);
        }
        return collections;
    }
}
