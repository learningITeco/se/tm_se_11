package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.endpoint.IProjectEndpoint;
import ru.potapov.tm.endpoint.ISessionEndpoint;
import ru.potapov.tm.endpoint.ITaskEndpoint;
import ru.potapov.tm.endpoint.IUserEndpoint;

import java.text.SimpleDateFormat;

public interface ServiceLocator {
    @NotNull IProjectEndpoint getProjectService();
    @NotNull ITaskEndpoint getTaskService();
    @NotNull ISessionEndpoint getSessionEndpoint();
    @NotNull IUserEndpoint getUserService();
    @NotNull ITerminalService getTerminalService();
    @NotNull SimpleDateFormat getFt();
}
