package ru.potapov.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.*;

import ru.potapov.tm.endpoint.*;
import ru.potapov.tm.repository.*;
import ru.potapov.tm.service.*;

import java.text.SimpleDateFormat;
import java.util.Scanner;
import javax.xml.ws.Endpoint;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class  Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository= new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository      = new TaskRepository();
    @NotNull private final IUserRepository userRepository      = new UserRepository();
    @NotNull private final ISessionRepository sessionRepository= new SessionRepository();

    @Nullable private ITerminalService terminalService = new TerminalService(this);
    @Nullable private IProjectEndpoint projectIEndpoint = new ProjectEndpoint(this, projectRepository);
    @Nullable private ITaskEndpoint taskIEndpoint = new TaskEndpoint(this, taskRepository);
    @Nullable private IUserEndpoint userIEndpoint = new UserEndpoint(this, userRepository);
    @Nullable private ISessionEndpoint sessionIEndpoint = new SessionEndpoint(this, sessionRepository);
    @Nullable private Endpoint projectEndpoint, taskEndpoint, userEndpoint, sessionEndpoint;

    @NotNull final SimpleDateFormat ft            = new SimpleDateFormat("dd-MM-yyyy");

    public void init() throws Exception{
        try {
            if (getUserService().sizeUserMap() == 0)
                getUserService().loadAllUsers();
        }catch (Exception e){ e.printStackTrace(); }
        if (getUserService().sizeUserMap() == 0)
            getUserService().createPredefinedUsers();

        startEndpoints();

        String command = "";  Scanner in = new Scanner(System.in);
        while (!"exit".equals(command)){
            System.out.println("Commands: <stop>, <start>, <exit>");
            command = in.nextLine();

            switch (command){
                case "start": try {
                    startEndpoints();
                } catch ( Exception e) { e.printStackTrace(); }
                finally { break; }
                case "stop": stopEndpoints();break;
            }
        }
        stopEndpoints();
    }

    private void startEndpoints() {
        projectEndpoint =  Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectIEndpoint);
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");

        taskEndpoint    = Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskIEndpoint);
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");

        userEndpoint    = Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userIEndpoint);
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");

        sessionEndpoint = Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionIEndpoint);
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
        System.out.println("Command <start> completed");
    }

    private void stopEndpoints() {
        projectEndpoint.stop();
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl - has stopped");

        taskEndpoint.stop();
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl - has stopped");

        userEndpoint.stop();
        System.out.println("http://localhost:8080/UserEndpoint?wsdl - has stopped");

        sessionEndpoint.stop();
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl - has stopped");
        System.out.println("Command <stop> completed");
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }

    @NotNull
    @Override
    public IProjectEndpoint getProjectService() {
        return projectIEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpoint getTaskService() {
        return taskIEndpoint;
    }

    @NotNull
    @Override
    public IUserEndpoint getUserService() {
        return userIEndpoint;
    }

    @NotNull
    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionIEndpoint;
    }
}