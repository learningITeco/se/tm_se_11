package ru.potapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {
    @WebMethod int checkTaskSize(@WebParam @NotNull Session session) throws ValidateExeption;
    @WebMethod @Nullable Task findTaskByName(@WebParam @NotNull Session session, @NotNull final String name) throws ValidateExeption;
    @WebMethod void removeTask(@WebParam @NotNull Session session, @NotNull final Task task) throws ValidateExeption;
    @WebMethod void removeAllTasksByUserId(@WebParam @NotNull Session session, @NotNull final String userId) throws ValidateExeption;
    @WebMethod void removeAllTasks(@WebParam @NotNull Session session, @NotNull final Collection<Task> listTasks) throws ValidateExeption;
    @WebMethod @NotNull Task renameTask(@WebParam @NotNull Session session, @NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption;
    @WebMethod void changeProject(@WebParam @NotNull Session session, @NotNull final Task task, @NotNull final Project project) throws CloneNotSupportedException, ValidateExeption;
    @WebMethod @NotNull Collection<Task> findAllTasks(@WebParam @NotNull Session session, @NotNull final String projectId) throws ValidateExeption;
    @WebMethod @NotNull Collection<Task> findAllTasksByUserId(@WebParam @NotNull Session session, @NotNull final String userId, @NotNull final String projectId) throws ValidateExeption;
    @WebMethod @NotNull Collection<Task> findAllByUser(@WebParam @NotNull Session session, @NotNull final String userId) throws ValidateExeption;
    @WebMethod void putTask(@NotNull Session session, @WebParam @NotNull Task task) throws ValidateExeption;
    @WebMethod @NotNull String collectTaskInfo(@WebParam @NotNull Session session, @NotNull final Task task) throws ValidateExeption;
    //@WebMethod
    void setTaskMapRepository(@NotNull final MyMap mapRepository);
    //@WebMethod
    @NotNull MyMap getTaskMapRepository();
    @WebMethod void loadBinar(@WebParam @NotNull Session session) throws Exception;
    @WebMethod void saveBinar(@WebParam @NotNull Session session) throws Exception;

    @WebMethod void saveJaxb(@WebParam @NotNull Session session, final boolean formatXml) throws Exception;
    @WebMethod void loadJaxb(@WebParam @NotNull Session session, final boolean formatXml) throws Exception;

    @WebMethod void saveFasterXml(@WebParam @NotNull Session session) throws Exception;
    @WebMethod void loadFasterXml(@WebParam @NotNull Session session) throws Exception;

    @WebMethod void saveFasterJson(@WebParam @NotNull Session session) throws Exception;
    @WebMethod void loadFasterJson(@WebParam @NotNull Session session) throws Exception;
}
