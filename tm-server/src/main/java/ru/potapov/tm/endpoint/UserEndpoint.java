package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractServiceEndpoint<User> implements IUserEndpoint {
    @Override
    public @Nullable RoleType getUserRole(@NotNull Session session, @NotNull User user) throws ValidateExeption {
        getServiceLocator().getSessionEndpoint().validSession(session);

        return user.getRoleType();
    }

    @Override
    public boolean isAdministrator(@NotNull Session session, @NotNull User user) throws ValidateExeption {
        if (!getServiceLocator().getSessionEndpoint().validSession(session))
            return false;

        if (getUserRole(session, user ).equals(RoleType.Administrator))
            return true;

        return false;
    }

    @Nullable private User authorizedUser  = null;
    private boolean isAuthorized           = false;

    public UserEndpoint(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<User> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception {
        @Nullable User user = getServiceLocator().getUserService().getUserByName(name);
        if (Objects.isNull(user) ){ return null; }

        String hashPass = SignatureUtil.sign(pass,"",1);

        if ( !isUserPassCorrect(user, hashPass) ){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed the user password!");
            return null;
        }

        Session session = new Session();
        session.setUserId(user.getId());
        getServiceLocator().getSessionEndpoint().generateSession(session, user);
        getServiceLocator().getSessionEndpoint().addSession(session, user.getId());
        user.setSession(session);
        return user;
    }

    @Override
    public void setUserMapRepository(@NotNull MyMap mapRepository) {
        getRepository().setMapRepository((Map<String, User>)mapRepository.getMap());
    }

    @Override
    public void saveAllUsers() throws Exception {
        @NotNull final File file = new File("users.bin");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));
        @NotNull final Map<String, User> mapObj = ( Map<String, User>) getUserMapRepository().getMap();
        inputStream.writeObject( mapObj );
    }

    @Override
    public int sizeUserMap() {
        return getUserCollection().size();
    }

    @Override
    public void loadAllUsers() throws Exception{
        @NotNull final File file = new File("users.bin");
        if (!file.canRead()){
            //getBootstrap().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream( new FileInputStream(file) );
        @NotNull final Object objMapUser = inputStream.readObject();
        if (objMapUser instanceof Map){
            @NotNull final Map<String, User> mapUser = (Map<String, User>) objMapUser;
            setUserMapRepository(new MyMap(mapUser) );
        }    }

    @Override
    public @NotNull MyMap getUserMapRepository() {
        return new MyMap(getRepository().getMapRepository());
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role){
        User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        getRepository().persist(user);

        return user;
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name){
        return getRepository().findOne(name);
    }

    @Nullable
    @Override
    public User getUserById(@Nullable Session session, @Nullable final String id) throws ValidateExeption{
        if (!getServiceLocator().getSessionEndpoint().validSession(session))
            return null;

        return getRepository().findOneById(id);
    }

    @Override
    public boolean isUserPassCorrect(@Nullable final User user, @Nullable final String hashPass){
        boolean res = false;

        //if ( Arrays.equals(user.getHashPass(), hashPass) )
        if (user.getHashPass().equals(hashPass))
            res = true;

        return res;
    }

    @NotNull
    @Override
    public Collection<User> getUserCollection(){
        return getRepository().getCollection();
    }

    @NotNull
    @Override
    public User changePass(@Nullable Session session, @Nullable final User user, @Nullable final String newHashPass) throws CloneNotSupportedException, ValidateExeption{
        if (!getServiceLocator().getSessionEndpoint().validSession(session))
            return null;

        User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        return getRepository().merge(newUser);
    }

    @Override
    public void putUser(@Nullable Session session, @Nullable final User user) throws ValidateExeption{
        if (!getServiceLocator().getSessionEndpoint().validSession(session))
            return;

        getRepository().persist(user);
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable Session session,@Nullable final User user) throws ValidateExeption{
        String res = "";
        if (!getServiceLocator().getSessionEndpoint().validSession(session))
            return res;

        res += "\n";
        res += "    User [" + user.getLogin() + "]" +  "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    @Override
    public void createPredefinedUsers() {
        String hashPass;

        hashPass = SignatureUtil.sign("1","",1);;
        createUser("user",  hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("1","",1);;
        createUser("user2",  hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("2","",1);;;
        createUser("admin", hashPass, RoleType.Administrator);
    }
}
