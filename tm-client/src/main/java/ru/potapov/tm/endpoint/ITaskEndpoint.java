package ru.potapov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-15T12:45:48.394+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.potapov.ru/", name = "ITaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ITaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadFasterXmlRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadFasterXmlResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadFasterXml/Fault/Exception")})
    @RequestWrapper(localName = "loadFasterXml", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadFasterXml")
    @ResponseWrapper(localName = "loadFasterXmlResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadFasterXmlResponse")
    public void loadFasterXml(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadJaxbRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadJaxbResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadJaxb/Fault/Exception")})
    @RequestWrapper(localName = "loadJaxb", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadJaxb")
    @ResponseWrapper(localName = "loadJaxbResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadJaxbResponse")
    public void loadJaxb(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        boolean arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/changeProjectRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/changeProjectResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/changeProject/Fault/ValidateExeption"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/changeProject/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "changeProject", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.ChangeProject")
    @ResponseWrapper(localName = "changeProjectResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.ChangeProjectResponse")
    public void changeProject(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.potapov.tm.endpoint.Task arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        ru.potapov.tm.endpoint.Project arg2
    ) throws ValidateExeption_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllByUserRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllByUserResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllByUser/Fault/ValidateExeption")})
    @RequestWrapper(localName = "findAllByUser", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindAllByUser")
    @ResponseWrapper(localName = "findAllByUserResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindAllByUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.potapov.tm.endpoint.Task> findAllByUser(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findTaskByNameRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findTaskByNameResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findTaskByName/Fault/ValidateExeption")})
    @RequestWrapper(localName = "findTaskByName", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindTaskByName")
    @ResponseWrapper(localName = "findTaskByNameResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.potapov.tm.endpoint.Task findTaskByName(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/collectTaskInfoRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/collectTaskInfoResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/collectTaskInfo/Fault/ValidateExeption")})
    @RequestWrapper(localName = "collectTaskInfo", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.CollectTaskInfo")
    @ResponseWrapper(localName = "collectTaskInfoResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.CollectTaskInfoResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String collectTaskInfo(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.potapov.tm.endpoint.Task arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/getTaskMapRepositoryRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/getTaskMapRepositoryResponse")
    @RequestWrapper(localName = "getTaskMapRepository", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.GetTaskMapRepository")
    @ResponseWrapper(localName = "getTaskMapRepositoryResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.GetTaskMapRepositoryResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.potapov.tm.endpoint.MyMap getTaskMapRepository();

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/putTaskRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/putTaskResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/putTask/Fault/ValidateExeption")})
    @RequestWrapper(localName = "putTask", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.PutTask")
    @ResponseWrapper(localName = "putTaskResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.PutTaskResponse")
    public void putTask(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.potapov.tm.endpoint.Task arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllTasksByUserIdRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllTasksByUserIdResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllTasksByUserId/Fault/ValidateExeption")})
    @RequestWrapper(localName = "findAllTasksByUserId", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindAllTasksByUserId")
    @ResponseWrapper(localName = "findAllTasksByUserIdResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindAllTasksByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.potapov.tm.endpoint.Task> findAllTasksByUserId(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.String arg2
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadFasterJsonRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadFasterJsonResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadFasterJson/Fault/Exception")})
    @RequestWrapper(localName = "loadFasterJson", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadFasterJson")
    @ResponseWrapper(localName = "loadFasterJsonResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadFasterJsonResponse")
    public void loadFasterJson(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeAllTasksRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeAllTasksResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeAllTasks/Fault/ValidateExeption")})
    @RequestWrapper(localName = "removeAllTasks", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RemoveAllTasks")
    @ResponseWrapper(localName = "removeAllTasksResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RemoveAllTasksResponse")
    public void removeAllTasks(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.util.List<ru.potapov.tm.endpoint.Task> arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeTaskRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeTaskResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeTask/Fault/ValidateExeption")})
    @RequestWrapper(localName = "removeTask", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RemoveTask")
    @ResponseWrapper(localName = "removeTaskResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RemoveTaskResponse")
    public void removeTask(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.potapov.tm.endpoint.Task arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/checkTaskSizeRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/checkTaskSizeResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/checkTaskSize/Fault/ValidateExeption")})
    @RequestWrapper(localName = "checkTaskSize", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.CheckTaskSize")
    @ResponseWrapper(localName = "checkTaskSizeResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.CheckTaskSizeResponse")
    @WebResult(name = "return", targetNamespace = "")
    public int checkTaskSize(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/setTaskMapRepositoryRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/setTaskMapRepositoryResponse")
    @RequestWrapper(localName = "setTaskMapRepository", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SetTaskMapRepository")
    @ResponseWrapper(localName = "setTaskMapRepositoryResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SetTaskMapRepositoryResponse")
    public void setTaskMapRepository(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.MyMap arg0
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeAllTasksByUserIdRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeAllTasksByUserIdResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/removeAllTasksByUserId/Fault/ValidateExeption")})
    @RequestWrapper(localName = "removeAllTasksByUserId", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RemoveAllTasksByUserId")
    @ResponseWrapper(localName = "removeAllTasksByUserIdResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RemoveAllTasksByUserIdResponse")
    public void removeAllTasksByUserId(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveFasterXmlRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveFasterXmlResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveFasterXml/Fault/Exception")})
    @RequestWrapper(localName = "saveFasterXml", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveFasterXml")
    @ResponseWrapper(localName = "saveFasterXmlResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveFasterXmlResponse")
    public void saveFasterXml(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/renameTaskRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/renameTaskResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/renameTask/Fault/ValidateExeption"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/renameTask/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "renameTask", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RenameTask")
    @ResponseWrapper(localName = "renameTaskResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.RenameTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.potapov.tm.endpoint.Task renameTask(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.potapov.tm.endpoint.Task arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.String arg2
    ) throws ValidateExeption_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveFasterJsonRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveFasterJsonResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveFasterJson/Fault/Exception")})
    @RequestWrapper(localName = "saveFasterJson", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveFasterJson")
    @ResponseWrapper(localName = "saveFasterJsonResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveFasterJsonResponse")
    public void saveFasterJson(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveJaxbRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveJaxbResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveJaxb/Fault/Exception")})
    @RequestWrapper(localName = "saveJaxb", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveJaxb")
    @ResponseWrapper(localName = "saveJaxbResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveJaxbResponse")
    public void saveJaxb(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        boolean arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllTasksRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllTasksResponse", fault = {@FaultAction(className = ValidateExeption_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/findAllTasks/Fault/ValidateExeption")})
    @RequestWrapper(localName = "findAllTasks", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindAllTasks")
    @ResponseWrapper(localName = "findAllTasksResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.FindAllTasksResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.potapov.tm.endpoint.Task> findAllTasks(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws ValidateExeption_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveBinarRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveBinarResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/saveBinar/Fault/Exception")})
    @RequestWrapper(localName = "saveBinar", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveBinar")
    @ResponseWrapper(localName = "saveBinarResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.SaveBinarResponse")
    public void saveBinar(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadBinarRequest", output = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadBinarResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.potapov.ru/ITaskEndpoint/loadBinar/Fault/Exception")})
    @RequestWrapper(localName = "loadBinar", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadBinar")
    @ResponseWrapper(localName = "loadBinarResponse", targetNamespace = "http://endpoint.tm.potapov.ru/", className = "ru.potapov.tm.endpoint.LoadBinarResponse")
    public void loadBinar(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.potapov.tm.endpoint.Session arg0
    ) throws Exception_Exception;
}
