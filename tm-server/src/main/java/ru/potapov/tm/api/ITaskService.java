package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;
import java.util.Map;

public interface ITaskService {
    int checkSize();
    @Nullable Task findTaskByName(@NotNull final String name);
    void remove(@NotNull final Task task);
    void removeAll(@NotNull final String userId);
    void removeAll(@NotNull final Collection<Task> listTasks);
    @NotNull Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException;
    void changeProject(@NotNull final Task task, @NotNull final Project project) throws CloneNotSupportedException;
    @NotNull Collection<Task> findAll(@NotNull final String projectId);
    @NotNull Collection<Task> findAll(@NotNull final String userId, @NotNull final String projectId);
    @NotNull Collection<Task> findAllByUser(@NotNull final String userId);
    void put(Task task);
    @NotNull String collectTaskInfo(@NotNull final Task task);
    void setMapRepository(@NotNull final Map<String, Task> mapRepository);
    @NotNull Map<String, Task> getMapRepository();
}
