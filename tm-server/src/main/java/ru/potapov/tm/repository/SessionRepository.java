package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ISessionRepository;
import ru.potapov.tm.entity.Session;

import java.util.Collection;
import java.util.Map;

public class SessionRepository extends AbstractRepository<Session>  implements ISessionRepository<Session> {
    public SessionRepository() {
        super();
    }

    @Override
    public void setMapRepository(@NotNull Map<String, Session> mapRepository) {
        super.setMapRepository(mapRepository);
    }

    @Override
    public @NotNull Map<String, Session> getMapRepository() {
        return super.getMapRepository();
    }

    @Override
    public @NotNull Collection<Session> findAll() {
        return super.findAll();
    }

    @Override
    public @NotNull Collection<Session> findAll(String userId) {
        return super.findAll(userId);
    }

    @Nullable
    @Override
    public Session findOne(@NotNull String name) {
        return super.findOne(name);
    }

    @Nullable
    @Override
    public Session findOne(@NotNull String userId, @NotNull String name) {
        return super.findOne(userId, name);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String id) {
        return super.findOneById(id);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String userId, @NotNull String id) {
        return super.findOneById(userId, id);
    }

    @Override
    public void persist(@NotNull Session session) {
        super.persist(session);
    }

    @Override
    public void persist(@NotNull String userId, @NotNull Session session) {
        super.persist(userId, session);
    }

    @NotNull
    @Override
    public Session merge(@NotNull Session session) {
        return super.merge(session);
    }

    @NotNull
    @Override
    public Session merge(@NotNull String userId, @NotNull Session session) {
        return super.merge(userId, session);
    }

    @Override
    public void remove(@NotNull Session session) {
        super.remove(session);
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Session session) {
        super.remove(userId, session);
    }

    @Override
    public void removeAll() {
        super.removeAll();
    }

    @Override
    public void removeAll(@NotNull Collection<Session> list) {
        super.removeAll(list);
    }

    @Override
    public void removeAll(@NotNull String userId) {
        super.removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull String userId, @NotNull Collection<Session> list) {
        super.removeAll(userId, list);
    }

    @Override
    public @NotNull Collection<Session> getCollection() {
        return super.getCollection();
    }

    @Override
    public @NotNull Collection<Session> getCollection(@NotNull String userId) {
        return super.getCollection(userId);
    }
}
