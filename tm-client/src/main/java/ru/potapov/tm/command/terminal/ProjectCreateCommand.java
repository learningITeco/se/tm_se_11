package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.Project;
import ru.potapov.tm.endpoint.Status;
import ru.potapov.tm.endpoint.User;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {
    public ProjectCreateCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Creates a new project";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @Nullable Project findProject = null;
        boolean circleForName = true;
        while (circleForName){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a name for a new project:");
            String name = getServiceLocator().getTerminalService().getIn().nextLine();

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.nonNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with the name [" + name + "] already exist");
                continue;
            }

            circleForName = false;
            getServiceLocator().getTerminalService().printMassageOk();
            findProject = new Project();
            findProject.setName(name);
        }

        getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a description for this project:");
        findProject.setDescription(getServiceLocator().getTerminalService().getIn().nextLine());

        //Date:
        @NotNull Date date =getServiceLocator().getTerminalService().inputDate("Input a start date for new project in format<dd-mm-yyyy>:");
        findProject.setDateStart(getServiceLocator().getTerminalService().DateToXml(date));

        date = getServiceLocator().getTerminalService().inputDate("Input a finish date for new project:");
        findProject.setDateFinish(getServiceLocator().getTerminalService().DateToXml(date));

        findProject.setId(UUID.randomUUID().toString());
        findProject.setUserId(getServiceLocator().getUserService().getAuthorizedUser().getId());
        findProject.setStatus(Status.PLANNED);

        getServiceLocator().getTerminalService().printMassageCompleted();
        getServiceLocator().getProjectService().put(findProject);

        @NotNull User owner = getServiceLocator().getUserService().getUserById(findProject.getUserId());
        if (Objects.isNull(owner))
            return;

        @NotNull String projectInfo = getServiceLocator().getProjectService().collectProjectInfo(findProject, owner.getLogin());
        getServiceLocator().getTerminalService().printlnArbitraryMassage(projectInfo);
    }
}
