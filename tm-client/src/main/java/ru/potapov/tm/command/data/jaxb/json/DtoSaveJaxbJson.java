package ru.potapov.tm.command.data.jaxb.json;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.Session;
import ru.potapov.tm.endpoint.User;

import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
public class DtoSaveJaxbJson extends AbstractCommand {
    public DtoSaveJaxbJson(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-jaxb-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in json-format by JAXB";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        @Nullable final User user = getServiceLocator().getUserService().getAuthorizedUser();
        if (Objects.isNull(user) || !getServiceLocator().getUserService().isAdministrator( user ))
            return;
        if (!super.allowedRun())
            return;

        getServiceLocator().getProjectService().saveJaxb(getServiceLocator().getSessionService().getSession(), false);
        getServiceLocator().getTaskService().saveJaxb(getServiceLocator().getSessionService().getSession(), false);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
