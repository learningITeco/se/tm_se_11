package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.time.Duration;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractServiceEndpoint<Session> implements ISessionEndpoint {
    @NotNull final Map<String, Session> mapSession = new HashMap<>();
    @Nullable private Bootstrap bootstrap;


    public SessionEndpoint(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Session> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public boolean validSession(@NotNull Session session) throws ValidateExeption {
        Session sessionClone;
        try { sessionClone = session.clone(); }catch (Exception e){return false;}
        sessionClone.setSignature("");
        String sign = SignatureUtil.sign(sessionClone,"",1);
        if ( sign.equals(session.getSignature()) ){
            long timDif = (new Date().getTime() - session.getDateStamp().getTime());
            if ( timDif < 86400000 )
                return true;
        }
        return false;
    }

    @Override
    public @Nullable Session generateSession(@NotNull Session session, @NotNull User user) {
        Session sessionClone;
        try { sessionClone = session.clone(); }catch (Exception e){return null;}
        sessionClone.setSignature("");
        session.setSignature(SignatureUtil.sign(sessionClone,"", 1));
        session.setUserId(user.getId());
        return session;
    }

    @Override
    public void addSession(Session session, String userId) throws ValidateExeption {
        getServiceLocator().getSessionEndpoint().validSession(session);
        getRepository().persist(userId, session);
    }

    @Override
    public @Nullable Collection<Session> getSessionCollection() {
        return getRepository().getMapRepository().values();
    }

    @Override
    public void removeSession(@NotNull Session session) {
        getRepository().remove(session);
    }
}
