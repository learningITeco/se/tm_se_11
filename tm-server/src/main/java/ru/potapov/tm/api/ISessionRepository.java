package ru.potapov.tm.api;


import ru.potapov.tm.entity.Session;

public interface ISessionRepository<T extends IEntity> extends IRepository<Session> {
}
