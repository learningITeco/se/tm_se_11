package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.Project;
import ru.potapov.tm.endpoint.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectReadCommand extends AbstractCommand {
    @NotNull SortBy sortBy                = SortBy.ByCreate;
    @Nullable String partOfNameOrDiscript = null;

    public ProjectReadCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        setNeedAuthorize(false);
        return "project-read";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads all projects. Sorts by create[-c], by date start[-ds], by date finish[-df], by date status[-s]. Find by part of name/descript [-f [part of seeking] ] ";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()) || Objects.isNull(getServiceLocator().getUserService().getAuthorizedUser())
            || Objects.isNull(getServiceLocator().getUserService().getAuthorizedUser().getId()))
            return;

        getServiceLocator().getTerminalService().printlnArbitraryMassage("All projects: \n");
        if (getServiceLocator().getProjectService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We have not any project");
            return;
        }

        if (Objects.nonNull(additionalCommands)){
            if (additionalCommands.length > 1){
                if ("-c".equals(additionalCommands[1])){
                    sortBy = SortBy.ByCreate;
                }else if ("-ds".equals(additionalCommands[1])){
                    sortBy = SortBy.ByDateStart;
                }else if ("-df".equals(additionalCommands[1])){
                    sortBy = SortBy.ByDateFinish;
                }else if ("-s".equals(additionalCommands[1])){
                    sortBy = SortBy.ByStatus;
                }
            }
            if (additionalCommands.length > 2){
                if ("-f".equals(additionalCommands[1])){
                     partOfNameOrDiscript = additionalCommands[2];
                }
            }
        }

        @Nullable User owner = null;
        @NotNull final List<Project> list = (List<Project>)getServiceLocator().getProjectService().getCollection( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        Collections.sort(list, getComparator(sortBy));

        for (@NotNull final Project project : list) {
            owner = getServiceLocator().getUserService().getUserById(project.getUserId());

            if (Objects.nonNull(partOfNameOrDiscript)){
                if (Objects.isNull(project.getName()) || Objects.isNull(project.getDescription()))
                    continue;
                if ( !partOfNameOrDiscript.equals("")  && (!project.getName().contains(partOfNameOrDiscript)) && (!project.getDescription().contains(partOfNameOrDiscript)) )
                    continue;
            }

            String projectInfo = getServiceLocator().getProjectService().collectProjectInfo(project, owner.getLogin());
            getServiceLocator().getTerminalService().printlnArbitraryMassage(projectInfo);
        }
    }

    @NotNull
    private Comparator<Project> getComparator(SortBy sortBy){
        @NotNull final Comparator<Project> comparator = new Comparator<Project>() {
            @Override
            public int compare(Project project1, Project project2) {

                if (Objects.isNull(project1.getDateStart()) || Objects.isNull(project2.getDateStart())
                        || Objects.isNull(project1.getDateFinish())||Objects.isNull(project2.getDateFinish())
                        ||Objects.isNull(project1.getStatus())||Objects.isNull(project2.getStatus()))
                    return 0;

                if (sortBy.equals(SortBy.ByDateStart)){
                    return project1.getDateStart().toGregorianCalendar().compareTo(project2.getDateStart().toGregorianCalendar());
                }else if (sortBy.equals(SortBy.ByDateFinish)){
                    return project1.getDateFinish().toGregorianCalendar().compareTo(project2.getDateFinish().toGregorianCalendar());
                }else if (sortBy.equals(SortBy.ByStatus)){
                    return project1.getStatus().compareTo(project2.getStatus());
                }
                return 0;
            }
        };
        return comparator;
    }
}
